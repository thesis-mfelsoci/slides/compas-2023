# Vers un solveur direct à base de tâches pour des systèmes linéaires FEM/BEM creux/denses

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/compas-2023/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/compas-2023/-/commits/master)

[Diaporama](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/compas-2023/compas-2023.pdf)

