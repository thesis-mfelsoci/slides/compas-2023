(list
 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (commit "c19cd9f9470a8d60f8c1f8e0ff26b9cdb0e083ad"))
 (channel
  (name 'guix-extra)
  (url "https://gitlab.inria.fr/mfelsoci/guix-extra.git")
  (commit "df919a7f66f171bcc267edce46b4668ce9be5514")))
