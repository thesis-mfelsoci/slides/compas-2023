(specifications->manifest
 (list "texlive"
       "biber"
       "python@3"
       "python-pygments"
       "inkscape"
       "emacs"
       "emacs-org"
       "emacs-ess"
       "which"
       "git"
       "bash"))
