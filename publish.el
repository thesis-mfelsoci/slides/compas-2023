(require 'org)
;; Global publishing functions
(require 'ox-publish)
;; LaTeX publishing functions
(require 'ox-latex)
;; Support for citations
(require 'oc)
(require 'oc-biblatex)

;; Load presets.
(load-file "compose-publish/latex-publish.el")
(load-file "compose-publish/babel.el")

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process
      (list "latexmk --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"))

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)))

;; Use BibLaTeX citation processor by default.
(setq org-cite-export-processors '((t biblatex)))

;; Load additional LaTeX packages allowing us to:
;;   - use scalable vector graphics (without exporting them through LaTeX),
(add-to-list 'org-latex-packages-alist '("inkscapelatex = false" "svg"))
;;   - use additional symbols,
(add-to-list 'org-latex-packages-alist '("" "pifont"))
;;   - configure custom colors,
(add-to-list 'org-latex-packages-alist '("" "xcolor"))
;;   - access additional math commands,
(add-to-list 'org-latex-packages-alist '("" "mathtools"))
;;   - draw TikZ pictures,
(add-to-list 'org-latex-packages-alist '("" "tikz"))
;;   - typeset border matrices,
(add-to-list 'org-latex-packages-alist '("" "kbordermatrix"))
;;   - use additional symbols,
(add-to-list 'org-latex-packages-alist '("" "tikzsymbols"))
;;   - use the simple Inria Beamer theme with the logos of sponsors.
(add-to-list 'org-latex-packages-alist '("sponsors" "beamerthemeinria"))

;; Include the Inria favicon in to the HTML header.
(setq org-html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://thesis-mfelsoci.gitlabpages.inria.fr/slides/postdoc/
favicon.ico\"/>")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "compas-2023"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["compas-2023.org"]
             :publishing-function '(org-beamer-publish-to-pdf-verbose-on-error)
             :publishing-directory "./public")
       (list "compas-2023-tikz"
             :base-directory "./figures/tikz"
             :base-extension "tex"
             :publishing-function '(latex-publish-to-pdf)
             :publishing-directory "./public")
       (list "compas-2023-figures"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["compas-2023.org"]
             :publishing-function '(org-babel-execute-file)
             :publishing-directory "./public")))

(provide 'publish)
